﻿#include <iostream>
using namespace std;

void schet(int arr[], int size) {

    int plus = 0, noll = 0, minus = 0;
    for (int i = 0; i < size; i++) {
        if (arr[i] > 0) {
            plus++;
        }
        else if (arr[i] < 0) {
            minus++;
        }
        else {
            noll++;
        }
    }

    cout << "Положительных " << plus << " элементов" << endl;
    cout << "Отрицательных " << minus << " элементов" << endl;
    cout << "Нулевых " << noll << " элементов" << endl;
}


int main()
{
    setlocale(LC_ALL, "ru");

    /*Написать функцию, определяющую количество положительных, отрицательных и нулевых
    элементов передаваемого ей массива.*/

    const int size = 10;
    int arr[size]{ 150,-30,50,0,16,-16,60,0,18,0 };
    schet(arr, size);



}

