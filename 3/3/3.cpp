﻿#include <iostream>
using namespace std;

void idea(int a, int b) {

    int summ = 0;

    for (int i = a; i <= b; i++) {
        for (int j = 1; j < i; j++) {
            if (i % j == 0) {
                summ += j;
            }
        }
        if (summ == i) {
            cout << i << " ";
        }
        summ = 0;
    }
}


int main()
{

    setlocale(LC_ALL, "ru");

    /*Число называется совершенным, если сумма всех его делителей равна ему самому. Напишите
    функцию поиска таких чисел во введенном интервале.*/


    int a, b;
    cout << "Введите начало интервала: ";
    cin >> a;
    cout << "Введите конец интервала: ";
    cin >> b;
    cout << endl;
    cout << "Совершенные числа интервала: от " << a << " до " << b << ": ";
    idea(a, b);
    cout << endl;


}
