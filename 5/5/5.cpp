﻿#include <iostream>
using namespace std;

bool happy(int a) {
    int summ1 = 0, summ2 = 0, n = a;
    for (int i = 0; i < 3; i++) {
        summ1 += n % 10;
        n /= 10;
    }
    for (int i = 0; i < 3; i++) {
        summ2 += n % 10;
        n /= 10;
    }

    return summ1 == summ2;
}


int main()
{
    
    setlocale(LC_ALL, "ru");

    int number;
    char replay = 'y';
    while (replay == 'y') {

        cout << "Введите шестизначное число" << endl;
        cin >> number;
        
        if (happy(number)) {
            cout << "Счастливое число" << endl << endl;
        }
        else {
            cout << "Не счастливое число" << endl << endl;
        }
        cout << "Для еще одного раза нажмите 'y' " << endl;
        cin >> replay;
    }

}
