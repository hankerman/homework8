﻿#include <iostream>
using namespace std;

int summ(int a, int b) {
    int sum = 0;
    for (int i = a; i <= b; i++) {
        sum += i;
    }
    return sum;
}



int main()
{
    setlocale(LC_ALL, "ru");

    /*Написать функцию, которая получает в качестве параметров 2 целых числа и возвращает сумму
    чисел из диапазона между ними*/

    int a, b;
    cout << "Введите начало диапозона: ";
    cin >> a;
    cout << endl;
    cout << "Введите конец диапазона: ";
    cin >> b;
    cout << endl;
    cout << "Сумма диапазона: " << summ(a, b) << endl;




}
