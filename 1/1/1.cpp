﻿#include <iostream>
using namespace std;

int step(int a, int b) {

    int s = 1;

    for (int i = 0; i < b; i++) {
        s = s * a;
    }
    return s;
}


int main()
{
    setlocale(LC_ALL, "ru");

    /*Написать функцию, которая принимает два параметра: основание степени и показатель
    степени, и вычисляет степень числа на основе полученных данных.
    */

    int a, b;

    cout << "Введите число: ";
    cin >> a;
    cout << endl;
    cout << "Введите степень: ";
    cin >> b;
    cout << endl;
    cout << "Результат = " << step(a, b) << endl;

}
