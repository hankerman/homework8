﻿#include <iostream>
using namespace std;

void zap(int arr[], int size) {

    for (int i = 0; i < size; i++) {
        arr[i] = rand();
    }

}

int sred(int arr[], int size) {

    int summ = 0, res;

    for (int i = 0; i < size; i++) {
        summ += arr[i];
    }
    res = summ / size;
    return res;
}


int main()
{
    
    setlocale(LC_ALL, "ru");
    srand(time(0));
    /*Написать функцию, определяющую среднее арифметическое элементов передаваемого ей
    массива.*/

    const int size = 10;
    int res;
    int arr[size];
    zap(arr, size);

    res = sred(arr, size);

    cout << "Средняя сумма масива равна: " << res << endl;

}
